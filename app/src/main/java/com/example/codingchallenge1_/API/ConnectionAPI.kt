package com.example.codingchallenge1_.API

import com.example.codingchallenge1_.Util.ResponseCallBack
import okhttp3.*
import java.io.IOException

object ConnectionAPI {

    // Get method

    fun startRequest(url: String,responseCallBack: ResponseCallBack) {
        val request = Request.Builder().url(url).build()
        val client = OkHttpClient()
        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                responseCallBack.onFailure()
            }
            override fun onResponse(call: Call, response: Response) {
                responseCallBack.onResponse(response)
            }
        })
    }
}