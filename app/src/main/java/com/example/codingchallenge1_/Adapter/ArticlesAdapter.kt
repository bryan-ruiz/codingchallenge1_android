package com.example.codingchallenge1_.Adapter

import android.content.Context
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.example.codingchallenge1_.Model.Article
import com.example.codingchallenge1_.R
import com.example.codingchallenge1_.Util.GetImageFromUrl
import com.example.codingchallenge1_.Util.StringUtil
import kotlinx.android.synthetic.main.item_article.view.*

class ArticlesAdapter(private val mContext: Context, private val articlesList: List<Article>) : ArrayAdapter<Article>(mContext, 0, articlesList) {

    // Variables

    lateinit var layoutItem: View

    // View

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        layoutItem = LayoutInflater.from(mContext).inflate(R.layout.item_article, parent, false)
        val article = articlesList[position]
        val stringUrl = StringUtil.textToSplit(article.image, "com/", true)
        val getImageFromUrl = GetImageFromUrl()
        setImage(getImageFromUrl, stringUrl)
        setTexts(article)
        return layoutItem
    }

    // Image

    private fun setImage(getImageFromUrl: GetImageFromUrl, stringUrl: String) {
        getImageFromUrl.GetImageFromUrl(layoutItem.imageView)
        getImageFromUrl.execute(stringUrl)

    }

    // Strings

    private fun setTexts(article: Article) {
        layoutItem.author.text = Html.fromHtml(StringUtil.boldText("Author", article.author))
        layoutItem.date.text = Html.fromHtml(StringUtil.boldText("Date", StringUtil.textToSplit(article.article_date, "+", false)))
        layoutItem.title.text = Html.fromHtml(StringUtil.boldText("Title", article.title))
        layoutItem.description.text = Html.fromHtml(StringUtil.boldText("Description", article.description))
    }
}