package com.example.codingchallenge1_.Fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.fragment.app.Fragment
import com.example.codingchallenge1_.API.ConnectionAPI
import com.example.codingchallenge1_.Adapter.ArticlesAdapter
import com.example.codingchallenge1_.MainActivity
import com.example.codingchallenge1_.Model.ArticleList
import com.example.codingchallenge1_.Util.ErrorDialog
import com.example.codingchallenge1_.Util.ResponseCallBack
import com.google.gson.Gson
import okhttp3.Response
import com.example.codingchallenge1_.R

class BlogFragment : Fragment() {

    // Variables

    private lateinit var articlesList: ArticleList
    private lateinit var listview: ListView
    private lateinit var adapter: ArticlesAdapter

    // Lifecycle

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_blog, container, false)
        listview = root.findViewById(R.id.blogList)
        setToolBar()
        setResponseCallBacks()
        return root
    }

    // Config

    private fun setToolBar() {
        (activity as MainActivity?)!!.setActionBarWithOutBackButton("Blog")
    }

    private fun setAdapter() {
        adapter = ArticlesAdapter(context!!, articlesList.articles)
        listview.adapter = adapter
    }

    // Listener

    private fun setListViewOnClickListener() {
        listview.setOnItemClickListener { parent, view, position, id ->
            val transaction = fragmentManager!!.beginTransaction()
            val fragment = LinkDetailFragment()
            val bundle = Bundle()
            bundle.putString("link", articlesList.articles[position].link)
            fragment.arguments = bundle
            transaction.replace(R.id.nav_host_fragment, fragment)
            transaction.addToBackStack(fragment.toString())
            transaction.commit()
        }
    }

    // API connection

    private fun setResponseCallBacks() {
        val responseCallBack = object : ResponseCallBack {
            override fun onResponse(response: Response) {
                val body = response.body()?.string()
                activity!!.runOnUiThread(java.lang.Runnable {
                    articlesList = Gson().fromJson(body, ArticleList::class.java)
                    setAdapter()
                    setListViewOnClickListener()
                })
            }

            override fun onFailure() {
                ErrorDialog.showError(context!!.resources.getString(R.string.title_error), context!!.resources.getString(R.string.message_try_again) , context!!)
            }
        }
        val url = "https://www.beenverified.com/articles/index.android.json"
        ConnectionAPI.startRequest(url, responseCallBack)
    }
}