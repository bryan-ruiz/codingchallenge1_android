package com.example.codingchallenge1_.Fragment

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.example.codingchallenge1_.MainActivity
import com.example.codingchallenge1_.Util.CustomWebViewClient
import com.example.codingchallenge1_.R


class LinkDetailFragment : Fragment() {

    // Variables

    private lateinit var webView: WebView

    // Lifecycle

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_link_detail, container, false)
        webView = root.findViewById(R.id.webView)
        setToolBar()
        loadWebView()
        return root
    }

    // Config

    private fun setToolBar() {
        (activity as MainActivity?)!!.setActionBarWithBackButton("Detail")
    }

    private fun loadWebView() {
        val link = arguments!!.getString("link")
        webView.loadUrl(link)
        webView.webViewClient = CustomWebViewClient()
    }
}