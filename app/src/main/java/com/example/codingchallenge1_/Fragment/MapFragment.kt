package com.example.codingchallenge1_.Fragment

import android.Manifest
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityCompat.requestPermissions
import androidx.fragment.app.Fragment
import com.example.codingchallenge1_.MainActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.example.codingchallenge1_.R
import com.example.codingchallenge1_.Util.ErrorDialog
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.MarkerOptions

class MapFragment : Fragment(), OnMapReadyCallback {

    // Variables

    val PERMISSION_REQUEST_CODE = 1
    private lateinit var mMap: GoogleMap
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location
    private lateinit var searEditText: EditText

    // Lifecycle

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_map, container, false)
        searEditText = root.findViewById(R.id.searchEditText)
        setToolBar()
        setMapFragment()
        setMapLocationProvider()
        return root
    }

    // Config

    private fun setMapFragment() {
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun setMapLocationProvider() {
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context!!)
    }

    private fun setToolBar() {
        (activity as MainActivity?)!!.setActionBarWithOutBackButton("Map")
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        setEditTextListener()
        setMapPermission()
    }

    // Listener

    private fun setEditTextListener() {
        searEditText.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH || event.action == KeyEvent.KEYCODE_ENTER) {
                 geoLocate()
                true
            }
            else{
                false
            }
        }
    }

    // Marks

    private fun addMarkers(addressList: MutableList<Address>) {
//      got this code from https://www.javatpoint.com/kotlin-android-google-map-search-location
        if (addressList.size == 0) {
            ErrorDialog.showError(context!!.resources.getString(R.string.title_error), context!!.resources.getString(R.string.message_place_not_found) , context!!)
        }
        else {
            for (place in addressList) {
                val position = LatLng(place.latitude, place.longitude)
                mMap.addMarker(MarkerOptions().position(position).title(place.featureName))
            }
        }
    }

    private fun geoLocate() {
//      got this code from https://www.javatpoint.com/kotlin-android-google-map-search-location
        try {
            val searchText = searEditText.text.toString()
            val geocoder = Geocoder(context!!)
            var addressList: MutableList<Address> = mutableListOf()
            addressList = geocoder.getFromLocationName(searchText, 5)
            mMap.clear()
            addMarkers(addressList)
        } catch (e: Exception) {
            Log.e(context!!.resources.getString(R.string.title_error), e.toString())
        }
    }

    // User Location

    fun setCurrentLocationOnMap() {
//      got this code from https://www.javatpoint.com/kotlin-android-google-map-search-location
        mMap.isMyLocationEnabled = true
        fusedLocationProviderClient.lastLocation.addOnSuccessListener {
            if (it != null) {
                lastLocation = it
                val currentLatLong = LatLng(it.latitude, it.longitude)
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLong, 13f))
            }
        }
    }

    // Permissions

    private fun setMapPermission() {
//      got this code from https://stackoverflow.com/questions/40760625/how-to-check-permission-in-fragment/40760760
        if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(activity!!, arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION), PERMISSION_REQUEST_CODE)
        } else {
            setCurrentLocationOnMap()
        }
    }
}