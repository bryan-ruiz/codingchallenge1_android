package com.example.codingchallenge1_

import android.content.pm.PackageManager
import android.os.Bundle
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.codingchallenge1_.Util.ErrorDialog
import com.example.codingchallenge1_.Fragment.MapFragment

class MainActivity : AppCompatActivity() {

    // Lifecycle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val navView: BottomNavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        val appBarConfiguration = AppBarConfiguration(setOf(R.id.navigation_blog, R.id.navigation_map))
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
    }

    // Permissions

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        supportFragmentManager.fragments.forEach {
            it.childFragmentManager.fragments.forEach { fragment ->
                if (fragment is MapFragment) {
                    if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                        fragment.setCurrentLocationOnMap()
                    } else {
                        ErrorDialog.showError(applicationContext.resources.getString(R.string.title_error), applicationContext.resources.getString(R.string.message_permission_not_granted), this)
                    }
                    return
                }
            }
        }
    }

    // Navigation

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    // Toolbar

    fun setActionBarWithOutBackButton(title: String) {
        val actionBar = supportActionBar
        actionBar!!.title = title
        actionBar.setDisplayHomeAsUpEnabled(false)
    }

    fun setActionBarWithBackButton(title: String) {
        val actionBar = supportActionBar
        actionBar!!.title = title
        actionBar.setDisplayHomeAsUpEnabled(true)
    }
}
