package com.example.codingchallenge1_.Model

// Article to use in view

class Article(val title:String, val description:String, val author:String, val image:String, val article_date:String, val link:String, val uuid:String) {

}