package com.example.codingchallenge1_.Util

import android.app.AlertDialog
import android.content.Context

object ErrorDialog {

    // Alert dialog

    fun showError(title: String, message: String, context: Context) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle(title)
        builder.setMessage(message)
        builder.setPositiveButton("Ok") { dialog, which ->
        }
        builder.show()
    }
}