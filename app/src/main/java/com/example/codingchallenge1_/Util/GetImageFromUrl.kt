package com.example.codingchallenge1_.Util

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.widget.ImageView
import java.net.URL

class GetImageFromUrl : AsyncTask<String, Void, Bitmap>() {

    // Variables

    lateinit var bitmap: Bitmap
    lateinit var imageView: ImageView

    // Constructor

    fun GetImageFromUrl(imageView: ImageView) {
        this.imageView = imageView
    }

    // Async methods

    override fun doInBackground(vararg params: String?): Bitmap {
        val url = params[0]
        try {
            val url = URL(url)
            bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream())
        } catch (e: Exception) {
            println(e.message)
        }
        return bitmap
    }

    override fun onPostExecute(result: Bitmap?) {
        super.onPostExecute(result)
        imageView.setImageBitmap(result)
    }
}