package com.example.codingchallenge1_.Util

import okhttp3.Response

interface ResponseCallBack {

    // Callback methods

    fun onResponse(response: Response)
    fun onFailure()
}