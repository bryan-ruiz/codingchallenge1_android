package com.example.codingchallenge1_.Util

object StringUtil {

    // Split

    fun textToSplit(text: String, dividerKey: String, validationIsImage: Boolean): String {
        val separatedText = text.split(dividerKey)
        return if (validationIsImage) {
            "${separatedText[0]}com/fit-in/60x/filters:autojpg()/${separatedText[1]}"
        } else {
            separatedText[0]
        }
    }

    // Bold

    fun boldText(title: String, description: String): String {
        return "<b>${title}: </b> ${description}"
    }
}